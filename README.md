# IntBibAdder

## Available

### Fields

*    address
*    annote
*    author
*    booktitle
*    chapter
*    crossref
*    edition
*    editor
*    howpublished
*    institution
*    journal
*    key
*    month
*    note

## Unavailable

### Entities

*    article
*    book
*    booklet
*    inbook
*    incollection
*    inproceedings
*    manual
*    mastersthesis
*    misc
*    phdthesis
*    proceedings
*    techreport
*    unpublished

### Fields

*    number
*    organization
*    pages
*    publisher
*    school
*    series
*    title
*    type
*    volume
*    year

